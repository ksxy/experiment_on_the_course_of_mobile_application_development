package com.cyy.personinfowithkt

/**
 * <pre>
 *     author : YYCHEN
 *     e-mail : xxx@xx
 *     time   : 2018/03/31
 *     desc   :
 *     version: 1.0
 * </pre>
 **/
data class PersonInfo(
        val stuName: String,
        val sex: String,
        val stuNum: Long,
        val college: String,
        val specialty: String,
        val url: String
)